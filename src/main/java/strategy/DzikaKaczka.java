package strategy;

import strategy.skills.*;

public class DzikaKaczka extends Kaczka {

    public DzikaKaczka(String name) {
        setName(name);
        interfejsLatanie = new NieLatam();
        interfejsKwakanie = new KwaczeCicho();
    }

    public void wyswietl(){
        System.out.println("nazywam sie " + getName());
    }

    public void plywaj() {
        System.out.println("Nie plywam.");
    }
}
