package strategy;

import strategy.skills.InterfejsKwakanie;
import strategy.skills.InterfejsLatanie;

public abstract class Kaczka {

    private String name;

    InterfejsKwakanie interfejsKwakanie;
    InterfejsLatanie interfejsLatanie;

    public void wykonajLec(){
        interfejsLatanie.lec();
    }

    public void wykonajKwacz(){
        interfejsKwakanie.kwacz();
    }

    public abstract void plywaj();

    public void setInterfejsKwakanie(InterfejsKwakanie interfejsKwakanie) {
        this.interfejsKwakanie = interfejsKwakanie;
    }

    public void setInterfejsLatanie(InterfejsLatanie interfejsLatanie) {
        this.interfejsLatanie = interfejsLatanie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
