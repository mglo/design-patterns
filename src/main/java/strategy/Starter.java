package strategy;

import strategy.skills.*;

public class Starter {

    public static void main(String[] args) {
        Kaczka dzika = new DzikaKaczka("DzikaKaczka");
        System.out.println(dzika.getName());
        dzika.wykonajKwacz();
        dzika.wykonajLec();
        dzika.plywaj();

        Kaczka model = new ModelKaczki("KapitanKaczka");
        model.setInterfejsKwakanie(new KwaczeGlosno());
        model.setInterfejsLatanie(new LatamSzybko());

        System.out.println(model.getName());
        model.wykonajLec();
        model.wykonajKwacz();
        model.plywaj();
    }


}
