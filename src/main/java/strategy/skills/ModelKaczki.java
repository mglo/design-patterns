package strategy.skills;

import strategy.Kaczka;

public class ModelKaczki extends Kaczka {


    public ModelKaczki(String name) {
        setName(name);
        InterfejsKwakanie ik = new KwaczeCicho();
        InterfejsLatanie il = new NieLatam();
    }


    public void plywaj() {
        System.out.println("Ja plywam!");
    }
}
